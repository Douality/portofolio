# Portfolio de Douality

![Bannière du portfolio](./rsc/banner.png)

## 📑 Table des matières

- [Introduction](#introduction)
- [Fonctionnalités](#fonctionnalités)
- [Technologies utilisées](#technologies-utilisées)
- [Sections](#sections)

---

## 🎉 Introduction

Bienvenue sur le dépôt de mon portfolio personnel. Ce portfolio présente un aperçu de mon parcours professionnel et académique, y compris mes projets et mon CV.

Pour voir le portfolio en action, visitez [Portfolio de Douality](https://douality.gitlab.io/portfolio/).

## ✨ Fonctionnalités

- Interface utilisateur intuitive et réactive
- Sections diverses pour une meilleure organisation de l'information :
    - Introduction
    - Études
    - Projets
    - CV
    - Contact

## 🛠 Technologies utilisées

- HTML5
- CSS3
- JavaScript
- Materialize CSS
- Google Fonts

## 📁 Sections

- **Introduction**: Une brève introduction à mon portfolio.
- **Études**: Informations sur mes études.
- **Projets**: Une galerie de projets sur lesquels j'ai eu la chance de travailler.
- **CV**: Un lien vers mon CV.
- **Contact**: Informations pour me contacter.

---
